const fs = require('fs');
const path=require('path');

const dir = path.join(__dirname,'jsonFiles');



function createRandomJsonFiles() { //for creating 10 json files

    for (let index = 0; index < 10; index++) {

        fs.writeFile(path.join(dir,`${index}.json`),'',(err)=>{

            if(err) 
                return console.log(`error : ${err}`);

        });
    }
};

function createOrDeleteJsonFiles(){
    fs.exists(dir, (isExists) => { //first checking if directory exists or not

        if (!isExists) { //if directory exists then true otherwise false

            fs.mkdir(dir,(err)=>{ //creating a directory

                if(err) 
                 return console.log('error : ',err);

            });

            createRandomJsonFiles(); //for creating json files

        } else {

            fs.rm(dir, { recursive: true },(err)=>{ //removing the directoryi and files inside it

                if(err) 
                 return console.log('error : ',err);

            });
        }
    });
}
module.exports=createOrDeleteJsonFiles;