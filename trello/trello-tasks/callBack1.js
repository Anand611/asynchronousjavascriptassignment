const boards=require('../trello-callbacks/boards.json');
function callBack1(boardId,cb){

 setTimeout(()=>{

      //if board id is not a string then it is not valid according to the given data
        if(typeof boardId !== 'string')
      return cb('Board id must be a string.');

    const board=boards.find((Board)=>boardId===Board.id);
   
    //if board with this boardId doesn't exists
    if(board===undefined)
      return cb('Invalid board id.');

    cb(board);

 },2*1000);
}

module.exports=callBack1;
