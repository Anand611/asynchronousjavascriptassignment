const callBack1=require('../callBack1');

const testCallBack1=()=>{

    //valid parameter passed
    callBack1('mcu453ed',(obj)=>{

        console.log(obj);

    });

    //invalid parameter passed
    callBack1('gbfjssd',(obj)=>{

        console.log(obj);

    });

    callBack1(undefined,(obj)=>{

        console.log(obj);

    });
}
testCallBack1();