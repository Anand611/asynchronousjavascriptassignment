const callBack2 = require('./callBack2');
const callBack3 = require('./callBack3');

const boards = require('../trello-callbacks/boards.json');


function callBack5() {

    const thanosBoard = boards.find((board) => board.name === 'Thanos'); //getting thanos board

    const thanosBoardId = thanosBoard.id; //extracting thanos board id from thanos board

    callBack2(thanosBoardId, (res) => {     //for getting lists associated with thanos board id

        const thanosLists = res;

        const mindListId = thanosLists.find(list => list.name === 'Mind').id; //extracting id of mind from all list associated with thanos
        const spaceListId=thanosLists.find(list => list.name === 'Space').id;////extracting id of space from all list associated with thanos

        callBack3(mindListId, (res) => { //for getting cards associated with mind

            const mindCards = res;
            
            console.log(thanosBoard,thanosLists,mindCards);
        });
        callBack3(spaceListId,(res)=>{ //for getting cards associated with space

            const spaceCards=res;
            
            console.log(spaceCards);

        })
    });
}

module.exports=callBack5;