const lists=require('../trello-callbacks/lists.json');
const boards=require('../trello-callbacks/boards.json');

function callBack2(boardId,cb){
    setTimeout(()=>{

        //if board id is not a string then it is not valid according to the given data
        if(typeof boardId !== 'string')
            return cb('Board id must be a string.');

        const board=boards.find((Board)=>boardId===Board.id);

        //if boardId doesn't exists 
        if(board===undefined) 
            return cb('Invalid board id.');
        
        const data=lists[board.id];//getting list associated with board id

        cb(data);

    },2*1000);
}

module.exports=callBack2;