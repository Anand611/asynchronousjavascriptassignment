const cards = require('../trello-callbacks/cards.json');


function callBack3(listId, cb) {
    setTimeout(() => {

        //if board id is not a string then it is not valid according to the given data
        if(typeof listId !== 'string')
            return cb('List id must be a string.');

        const allCards = cards[listId]; // getting all cards associated with the given list id

        if (allCards === undefined) //if there is no card exists with the given list id
         return cb('Invalid list id.');

        cb(allCards);
    }, 2 * 1000);
}

module.exports=callBack3;